import java.util.Scanner;

public class Sqrt {
    public static int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        int left = 1, right = x;
        int result = 0;

        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (mid == x / mid) {
                return mid;
            } else if (mid < x / mid) {
                left = mid + 1;
                result = mid;
            } else {
                right = mid - 1;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Input: 4");
        System.out.println("Output: "+mySqrt(4));  // Output: 2
        System.out.println("Input: 8");
        System.out.println("Output: "+mySqrt(8));  // Output: 2
        System.out.print("Input: ");
        int num = kb.nextInt();
        System.out.println("Output: "+mySqrt(num));
    }
}
